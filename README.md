# This is the [adapTive Elliptical Aperture PHOTometry (TEA-Phot)](https://bitbucket.org/DominicBowman/tea-phot) repository

**TEA-Phot** is a user-friendly pipeline to process ground-based photometry and extract light curves of stars for the purpose of variability and asteroseismology. **TEA-Phot** is coded in python and has no external software  dependencies such as IRAF, DS9 or Gaia, and utilises the
 flexible python module [SEP](https://sep.readthedocs.io/en/stable/) at its core. The code is optimised for high-cadence ground-based photometry, such as that provided by the SHOC and STE instruments at the [South African Astronomical Observatory (SAAO)](https://www.saao.ac.za), but can be easily modified for different instruments at different observatories.

**TEA-Phot** is an open-source package, GNU-licensed, and any improvements provided by the users are well accepted. See disclaimer below and GNU
 License in gpl-3.0.txt.

---
## Get **TEA-Phot**
**TEA-Phot** is available on

- [https://bitbucket.org/DominicBowman/tea-phot](https://bitbucket.org/DominicBowman/tea-phot)

You can quickly clone this repository by typing
```
git clone https://bitbucket.org/DominicBowman/tea-phot.git
```

A yml file is available to create a conda environment for TEA-Phot, but before installing, the contents of the yml file must be modified. Specifically, the line at the bottom must be modified to specify the path to your miniconda3 or anaconda3.

To create the TEA-Phot conda environment, using miniconda3 or anaconda3, run:
```
conda env create -f teaphot.yml
```

An operating system specific list of packages within the TEA-Phot conda environment of the developer (DMB) can also be found in the associated `teaphot_pkgs.txt' file.



---
## Authors

Written and developed by Dominic Bowman
```
 dominic.bowman@kuleuven.be
 Institute of Astronomy
 KU Leuven, Belgium
```

and Daniel Holdsworth
```
 dlholdsworth@uclan.ac.uk
 Jeremiah Horrocks Institute
 University of Central Lancashire, UK
```

---
## Citation

If you use the TEA-Phot code to produce results for a scientific publication, we ask you to please cite the TEA-Phot paper:

**Bowman & Holdsworth, (2019), A&A 629, A21**

URL: https://ui.adsabs.harvard.edu/abs/2019A%26A...629A..21B/abstract

We also strongly urge users of the TEA-Phot code to adhere to the citation requests of external python modules:

- [SEP](https://sep.readthedocs.io/en/stable/) paper:

**Barbary, (2016), Journal of Open Source Software, 1(6), 58**

URL: https://joss.theoj.org/papers/10.21105/joss.00058

- [astropy](https://www.astropy.org/acknowledging.html)

**Astropy Collaboration, (2013), A&A, 558, A33**
**Astropy Collaboration, (2018), AJ 156, 123**

URL (paper 1): https://ui.adsabs.harvard.edu/abs/2013A%26A...558A..33A/abstract
URL (paper 2): https://ui.adsabs.harvard.edu/abs/2018AJ....156..123A/abstract

- [matplotlib](https://matplotlib.org/citing.html)

**Hunter, J. D. 2007, Comput. Sci. Eng., 9, 90**

URL: https://ieeexplore.ieee.org/document/4160265


---
## Disclaimer

**TEA-Phot** is provided "as it is", without any warranty. The Authors assume no liability for any damages of any kind (direct or indirect damages, contractual or non-contractual damages, pecuniary or non-pecuniary damages), directly or indirectly derived or arising from the correct or incorrect usage of **TEA-Phot**, in any possible environment, or arising from the impossibility to use, fully or partially, the software, or any bug or malfunction. Such exclusion of liability expressly includes any damages including the loss of data of any kind (including personal data).
